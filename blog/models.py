from __future__ import unicode_literals

from django.db import models

class posts(models.Model):

    def __str__(self):
        return self.author

    author = models.CharField(max_length=30)
    title = models.CharField(max_length=100)
    bodytext = models.TextField()
    timestamp = models.DateTimeField()

