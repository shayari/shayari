from django.shortcuts import render_to_response
from blog.models import posts

def home(request):
    entries = posts.objects.all()[:10]
    # content = {
    #     'title':'My First Post',
    #     'author': 'irfan',
    #     'date': '27th May 1989',
    #     'body': 'lorem ipsum dolor sit amet, lorem ipsum dolor sit amet, lorem ipsum dolor sit amet, lorem ipsum dolor sit amet, lorem ipsum dolor sit amet,'
    #             ' lorem ipsum dolor sit amet, lorem ipsum dolor sit amet, lorem ipsum dolor sit amet, lorem ipsum dolor sit amet, lorem ipsum dolor sit amet,'
    #             ' lorem ipsum dolor sit amet, lorem ipsum dolor sit amet, lorem ipsum dolor sit amet, lorem ipsum dolor sit amet, lorem ipsum dolor sit amet,'
    #             ' lorem ipsum dolor sit amet, lorem ipsum dolor sit amet, lorem ipsum dolor sit amet, lorem ipsum dolor sit amet, lorem ipsum dolor sit amet, '
    # }
    return render_to_response('index.html',{'posts' : entries})
